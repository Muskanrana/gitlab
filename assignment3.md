Create and delete ninja directory on host machine
![](images/1.png)
![](images/2.png)

Set hostname on all nodes from remote machine
![](images/3.png)
![](images/4.png)

Fetch os of all nodes and store o/p into a file, use min two different machine of different flavour of os.
![](images/5.png)

Add three group into hosts file through ansible module.
          Debian ( ip of debian machine)
          Centos ( ip of centos machine)
          All ( ip of all nodes )
![](images/6.png)

* Install apache on Debian machine
![](images/7.png)

    * Cross check apache isntalled or not from remote machine
 ![](images/8.png)
    * Apache runn  on 8082 port
 ![](images/9.png)
  Create two virtual host
    * Restart apache from remote machine
 ![](images/10.png)
 ![](images/11.png)
 ![](images/12.png)
 ![](images/13.png)
 ![](images/14.png)
 ![](images/15.png)
 ![](images/16.png)
 ![](images/17.png)
 ![](images/18.png)
 ![](images/19.png)
  ![](images/21.png)
 ![](images/20.png)
 * Install nginx on centos machine
   * Nginx run on 8080 port.
Step3:
   * Configure Nginx - configure it to run as reverse proxy to apache
 ![](images/27.png)
 ![](images/22.png)
 ![](images/23.png)
 ![](images/24.png)
 ![](images/25.png)
 ![](images/26.png)






~

